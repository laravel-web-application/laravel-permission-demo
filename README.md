<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel Role Permission
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-permission-demo.git`
2. Go inside the folder: `cd laravel-permission-demo`
3. Run `cp .env.example .env` then put your DB name & credentials on that file.
4. Run `composer install`
5. Run `php artisan migrate --seed`
6. Run `php artisan serve`
7. Open your favorite browser: http://localhost:8000

### Screen shot

Home Page

![Home Page](img/home.png "Home Page")

Register Page

![Register Page](img/register.png "Register Page")

Login Page

![Login Page](img/login.png "Login Page")

Home Page

![Home Page](img/home2.png "Home Page")

Users Page

![Users Page](img/users.png "Users Page")

Permission Page

![Permission Page](img/permissions.png "Permission Page")

Roles Page

![Roles Page](img/roles.png "Roles Page")

Posts Page

![Posts Page](img/posts.png "Posts Page")
